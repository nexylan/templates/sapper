# Sapper

A Docker ready to use Sapper project.

- Demo: https://sapper.templates.nexylan.dev/
- Sapper website: https://sapper.svelte.dev/

## Requirements

- Docker

## New project setup

Get the download link of the latest version from the [releases page](-/releases).

```bash
wget https://gitlab.com/nexylan/templates/sapper/-/archive/master/sapper-master.tar.gz
tar xf sapper-master.tar.gz
rm sapper-master.tar.gz
mv sapper-master my-project
cd my-project
git init .
```

## Usage

Run:

```
make
```

Then click on the given url at the end on the output.

And voilà, you are ready to rock. 🎸

## Deployment

See: https://gitlab.com/nexylan/templates/docs/tree/v1.0.0#deployment
