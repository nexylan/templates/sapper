#!/usr/bin/env sh
set -e

wait-for-it app:3000 --timeout=60
cypress run
