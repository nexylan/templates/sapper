#!/usr/bin/env sh
set -e

echo "env: ${NODE_ENV?}"
if [ "${NODE_ENV?}" = "production" ]; then
	npm run start
else
	npm run dev
fi
