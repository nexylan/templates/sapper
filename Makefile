all: install up

install:
	npm install

up:
	npm run dev

test: install
	npm run test

ui:
	npm run cy:open
