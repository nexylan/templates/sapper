import posts from './_posts';

const contents = JSON.stringify(posts.map((post) => ({
  title: post.title,
  slug: post.slug,
})));

// eslint-disable-next-line import/prefer-default-export
export function get(req, res) {
  res.writeHead(200, {
    'Content-Type': 'application/json',
  });

  res.end(contents);
}
